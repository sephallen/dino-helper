# Dino Helper Discord Bot

Discord bot that helps finding dinos on you servers and reminding you anything you want.

## Setting up the bot

Requirements

* NodeJS
* npm
* Discord account
* A running ArkBot server https://github.com/ark-mod/ArkBot

Discord bot creation

* Create an app and a bot on https://discordapp.com/developers/applications
* Invite the bot to your discord server (
https://discordapp.com/oauth2/authorize?&client_id=CLIENT_ID&scope=bot&permissions=8)

Installation

* Clone this repository or download the source code
* Edit the config.json file (bot token, maps and wildcreatures API base url)

Config example

```
{
    "token" : "...",
    "base_url" : "https://my-arkbot-server-instance:7777/api/wildcreatures/",
    "maps" : {
        "isl": "theisland",
        "abe": "aberration",
        "rag": "ragnarok"
    }
}
```

* Run the following commands to start the bot

With pm2 (https://pm2.keymetrics.io/)

```
npm i --production
npm i -g pm2
pm2 start bot.js --name "dino-helper-bot"
```

Without pm2

```
npm i --production
node bot.js
```

## Usage

Find a dino

```
!dino <map> <dino_name>

!dino isl alpha
!dino rag mantis
```

Remind something

```
!remind <minutes> <message to remind>

!remind 10 feed the babies
```

