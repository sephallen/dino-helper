
const Discord = require('discord.js');
const logger = require('winston');
const table = require('text-table');
const { quote, getJson, findDinos } = require('./utils');
const { base_url, maps, token, loggerLevel } = require('./config.json');
const prefix = '!';

logger.remove(logger.transports.Console);

logger.add(new logger.transports.Console, {
    colorize: true
});

logger.level = loggerLevel;

logger.info('Connecting');

const client = new Discord.Client({ intents: ['GUILDS', 'GUILD_MESSAGES'] });

client.login(token);

client.on('ready', () => {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(`${client.user.username} (${client.user.id})`);
});

client.on('messageCreate', function (message) {
    if (message.author.bot) return;
    if (!message.content.startsWith(prefix)) return;

    const commandBody = message.content.slice(prefix.length);
    const args = commandBody.split(' ');
    const command = args.shift().toLowerCase();

    switch (command) {
        case 'dino':

            const map = args[0];
            const dino = args[1];

            if (map === 'help' || !dino || !maps[map]) {

                let response = 'Usage :\n';
                response += '!dino <map> <dino>\n';
                response += `Available maps:\n${Object.keys(maps).map(k => '- ' + k + ' : ' + maps[k]).join('\n')}`;

                message.reply(quote(response));
                break;
            }

            const url = `${base_url}${maps[map]}?t=${Date.now()}`;

            logger.info(`Request: ${url}`);

            getJson(url).then(data => {
                const dinos = findDinos(data, dino);

                logger.info(`Results: ${dinos.length} species`);

                if (!dinos.length) {
                    message.reply(quote(`Sorry, no results for ${dino} on ${map}`));
                }

                dinos.forEach(dinoList => {
                    const rows = [['Lvl', 'Gender', 'Lat', 'Lon']];

                    dinoList.Creatures.slice(0, 10).forEach(dino => {
                        rows.push([dino.BaseLevel, dino.Gender, dino.Latitude.toFixed(1), dino.Longitude.toFixed(1)]);
                    });

                    message.reply(quote(`${dinoList.Creatures.length} ${dinoList.Name || dinoList.ClassName}: \n${table(rows)}`));
                });
            });
            break;

        case 'remind':

            const usage = () => {
                message.reply(quote('Usage :\n!remind <minutes> <message>'));
            }

            if (args[0] === 'help' || !args[1]) {
                usage();
                break;
            }

            let timeout;

            try {
                let minutes = parseFloat(args[0]);
                if (minutes > 24 * 60) {
                    message.reply(quote('Cannot register timers longer than 24h'));
                    break;
                }
                timeout = minutes * 60 * 1000;
            } catch (err) {
                usage();
                break;
            }

            message.reply(quote('Timer registered'));

            const sentence = args.splice(2, args.length - 1).join(' ');

            setTimeout(() => {
                message.reply(quote(sentence));
            }, timeout);

            break;

        default:
            break;
    }
});
